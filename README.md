# React Native Chat App using Firebase
React Native is a JavaScript framework for writing real, natively rendering mobile applications for iOS and Android. It’s based on React, Facebook’s JavaScript library for building user interfaces, but instead of targeting the browser, it targets mobile platforms. In other words: web developers can now write mobile applications that look and feel truly “native,” all from the comfort of a JavaScript library that we already know and love. Plus, because most of the code you write can be shared between platforms, React Native makes it easy to simultaneously develop for both Android and iOS.

Firebase is a toolset to "build, improve, and grow your app", and the tools it gives you cover a large portion of the services that developers would normally have to build themselves, but don’t really want to build, because they’d rather be focusing on the app experience itself. This includes things like analytics, authentication, databases, configuration, file storage, push messaging etc. The services are hosted in the cloud, and scale with little to no effort on the part of the developer.


## Command to create Project (not required for git clone)
```javascript
expo init react-native-todo-app
```

## Commands to install Project
```javascript
sudo npm install expo-cli --global
sudo npm install react-navigation react-navigation-stack react-native-gesture-handler@~1.3.0
sudo npm install @react-native-community/masked-view react-native-safe-area-context
sudo npm install react-native-gesture-handler react-native-screens
sudo npm install firebase
sudo npm install react-native-gifted-chat
```

## Run Project
```javascript
expo start
```