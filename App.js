import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {LogBox} from 'react-native';

import LoginScreen from './screens/LoginScreen'
import ChatScreen from './screens/ChatScreen'

const AppNavigator = createStackNavigator(
  {
    Login: LoginScreen,
    Chat: ChatScreen
  },
  {
    headerMode: "none"
  }
)
LogBox.ignoreLogs(['Warning: AsyncStorage|Setting a timer'])

export default createAppContainer(AppNavigator);